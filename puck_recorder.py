
import json

import numpy as np

from datetime import datetime
from scipy.spatial.transform import Rotation

from vendor import triad_openvr

# Coverts euler pose to multidimensional array


def pose_matrix_to_numpy(pose_matrix):
    try:
        pose_arr = np.zeros((4, 4))
        pose_arr[0] = pose_matrix[0]
        pose_arr[1] = pose_matrix[1]
        pose_arr[2] = pose_matrix[2]
        pose_arr[3] = [0, 0, 0, 1]
        return pose_arr
    except:
        return []


convert_coordinate_system = np.identity(4)
convert_coordinate_system[:3, :3] = Rotation.from_euler(
    'XYZ', (180, 0, 0), degrees=True).as_matrix()
camera_to_puck_transforms = {}
###################################################################################
print("\n\nLooking for MFPucks...")
print("==============================================")
v = triad_openvr.triad_openvr("config.json")
v.print_discovered_objects()

# Settings from config.json
config = json.load(open("config.json", "r"))
blur_threshold = config["settings"]["blur_rejection_threshold"]
interval = config["settings"]["interval_in_seconds"]
device_name = config["settings"]["use_device"]
camera_index = config["settings"]["camera_index"]
colors = config["colors"]

csv = open("output.csv", 'w')
try:
    while True:
        if(device_name in v.devices):
            pose_mat = v.devices[device_name].get_pose_matrix()
            world_to_puck = pose_matrix_to_numpy(pose_mat)
            world_to_cams = {id_: world_to_puck @ head_to_cam @ convert_coordinate_system for (
                id_, head_to_cam) in camera_to_puck_transforms.items()}
            now = datetime.now()
            d = world_to_puck
            csv.write(
                f"{now},{d[0][0]},{d[0][1]},{d[0][2]},{d[0][3]},{d[1][0]},{d[1][1]},{d[1][2]},{d[1][3]},{d[2][0]},{d[2][1]},{d[2][2]},{d[2][3]},{d[3][0]},{d[3][1]},{d[3][2]},{d[3][3]}\n")
            # print(world_to_puck)
except KeyboardInterrupt:
    pass
